#!/usr/bin/python
import thread
import socket
import time
import cStringIO

running = True;

def request_segment(host, port, location):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, port))
    s.sendall('GET %s HTTP/1.1\r\n\r\n' % location)

    read_size = 500 * 1024;
    while True:
        response = cStringIO.StringIO()
        response.write(s.recv(read_size))
        print "[%s:%d%s] recv %dKB\n" % (host, port, location, read_size/1024);
        time.sleep(0.8);
    print "data len %d" % response.tell()
    s.close()

if __name__ == "__main__":
    thread.start_new_thread(request_segment, ("192.168.61.25", 20129, "/channel/0/encoder/0"));
    thread.start_new_thread(request_segment, ("192.168.61.25", 20129, "/channel/0/encoder/1"));
    thread.start_new_thread(request_segment, ("192.168.61.25", 20129, "/channel/1/encoder/0"));
    thread.start_new_thread(request_segment, ("192.168.61.25", 20129, "/channel/1/encoder/1"));
    thread.start_new_thread(request_segment, ("192.168.61.25", 20129, "/channel/2/encoder/0"));
    thread.start_new_thread(request_segment, ("192.168.61.25", 20129, "/channel/2/encoder/1"));
    while running:
        ch = raw_input();
        if(ch == "q"):
            running = False;
            break;
